var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require("multer");
var fs = require("fs");
var sqlite3 = require('sqlite3').verbose();


var db = new sqlite3.Database(path.resolve(__dirname, '../db/corpus.db'),
    sqlite3.OPEN_READWRITE,
    (err) => {
        if (err) { console.error(err.message); }
    });

var fileFilterImage = (req, file, cb) => {
    const allowedTypes = ["image/jpeg", "image/jpg", "image/png"];
    if (!allowedTypes.includes(file.mimetype)) {
        const error = new Error("Incorrect file");
        error.code = "INCORRECT_FILETYPE";
        return cb(error, false)
    }
    cb(null, true);
}
var fileFilterAudio = (req, file, cb) => {
    const allowedTypes = ["audio/wav"];
    if (!allowedTypes.includes(file.mimetype)) {
        const error = new Error("Incorrect file");
        error.code = "INCORRECT_FILETYPE";
        return cb(error, false)
    }
    cb(null, true);
}

var storageAudio = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'src/public/uploadsAudio')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
})

var storageImage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'src/public/uploadsImage')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
})

var uploadAudio = multer({
    storage : storageAudio,
    fileFilter: fileFilterAudio,
    // limits: {
    //     fileSize: 5000000
    // }
});
var uploadImage = multer({
    storage : storageImage,
    fileFilter: fileFilterImage,
    // limits: {
    //     fileSize: 5000000
    // }
});

/* GET home page. */

router.get('/search', function(req, res, next) {
    res.render('search', { title: 'Поиск по корпусу'});
});

router.get('/', function(req, res, next) {
    let fileContent = fs.readFileSync("index.html", "utf8");
    res.render('index', { title: 'Главная', html: fileContent});
});

router.get('/editMain', function(req, res, next) {
    let fileContent = fs.readFileSync("index.html", "utf8");
    res.render('editIndex', { title: 'Главная', html: fileContent});
});

router.get('/editMain/getmainpage', function(req, res, next) {
    let fileContent = fs.readFileSync("index.html", "utf8");
    res.status(200).json(fileContent);
});

router.post('/editMain/setmainpage', function(req, res, next) {
    fs.writeFileSync("index.html", req.body.html)
    res.send();
});

router.post('/editMain/sendimage', uploadImage.single('file'), function(req, res, next) {
    res.status(200).json({"location": req.file.path.substring(10, req.file.path.length)});
});

router.get('/editTable/:id', function(req, res, next) {
    res.render('editStory', { title: 'Детали рассказа'});
});

router.get('/editTable/:id/gettable', function(req, res, next) {
    db.all(`SELECT story_content.*, story.name as storyName, story.corpus_id as corpus_id
            FROM story_content
            INNER JOIN story ON story.id=story_content.story_id
            WHERE story_id=?`, req.params.id, (err, rows) => {
        if (err) { console.error(err.message); }
        var result = [];
        var storyName="";
        storyName = rows[0].storyName;
        rows.forEach(row => {
            result.push({ id: row.id, time_begin: row.time_begin, time_end: row.time_end,
                transcription: row.transcription, comment: row.comment, story_id: row.story_id, storyName: row.storyName,
                corpus_id: row.corpus_id });
        });
        db.all('SELECT link_on_file FROM story WHERE id=?', req.params.id, (err, rows) => {
            if (err) { console.error(err.message); }
            var link_on_file = "";
            link_on_file = rows[0].link_on_file;
            res.send({list: result, id: req.params.id, link_on_file: link_on_file, storyName: storyName});
        });

    });
});

router.post('/addTable', function(req, res, next) {
    console.log(req.body)
    db.run(`INSERT INTO story_content(time_begin, time_end, transcription, comment, story_id) VALUES ('${req.body.time_begin}', '${req.body.time_end}', '${req.body.transcription}', '${req.body.comment}', '${req.body.story_id}');`,
        function (err) {
            if (err) { console.error(err.message); }
            // db.get("SELECT last_insert_rowid() as id", function (err, row) {
            //     console.log('Last inserted id is: ' + row['id']);
            //     res.send(row['id']);
            // });
            res.status(200).json(this.lastID);
            //res.send(this.lastID);
            // res.redirect('/');
        }
    );
});

router.put('/updateTable/:id', function(req, res, next) {
    console.log(req.body)
    db.run(`UPDATE story_content SET time_begin='${req.body.time_begin}', time_end='${req.body.time_end}', transcription='${req.body.transcription}', comment='${req.body.comment}', story_id='${req.body.story_id}' WHERE id=?;`, req.params.id,
        (err) => {
            if (err) { console.error(err.message); }
            // res.redirect('/');
            res.send();
        }
    );
});

router.delete('/deleteTable/:id', function(req, res, next) {
    db.run(`DELETE FROM story_content WHERE id=?;`, req.params.id,
        (err) => {
            if (err) { console.error(err.message); }
            // res.redirect('/');
            res.send();
        }
    );
});

router.get('/corpuses', function(req, res, next) {
    db.all('SELECT * FROM corpus', (err, rows) => {
        if (err) { console.error(err.message); }
        //res.send(rows)
        var result = [];
        rows.forEach(row => {
            result.push({ id: row.id, name: row.name, description: row.description });
        });
        res.render('corpuses', { title: 'Корпуса', list: result});
    });
});

router.get('/editCorpuses', function(req, res, next) {
    res.render('editCorpuses', { title: 'Корпуса'});
});

router.get('/editCorpuses/getcorpuses', function(req, res, next) {
    db.all('SELECT * FROM corpus', (err, rows) => {
        if (err) { console.error(err.message); }
        res.send(rows)
    });
});

router.get('/corpus/:id', function(req, res, next) {
    db.all('SELECT * FROM story WHERE corpus_id=?', req.params.id, (err, rows) => {
        if (err) { console.error(err.message); }
        var result = [];
        rows.forEach(row => {
            result.push({ id: row.id, name: row.name, description: row.description });
        });
        res.render('corpus', { title: 'Список рассказов', list: result, id: req.params.id});
    });
});


router.get('/editCorpus/:id', function(req, res, next) {
    res.render('editCorpus', { title: 'Список рассказов'});
});

router.get('/editCorpus/:id/getcorpus', function(req, res, next) {
    db.all('SELECT * FROM story WHERE corpus_id=?', req.params.id, (err, rows) => {
        if (err) { console.error(err.message); }
        var result = [];
        rows.forEach(row => {
            result.push({ id: row.id, name: row.name, description: row.description });
        });
        res.send({list: result, id: req.params.id});
    });
});


router.post('/editCorpuses/addCorpus', function(req, res, next) {
    console.log(req.body)
    db.run(`INSERT INTO corpus(name, description, full_description) VALUES ('${req.body.name}', '${req.body.description}', '${req.body.full_description}');`,
        function (err) {
            if (err) { console.error(err.message); }
            // db.get("SELECT last_insert_rowid() as id", function (err, row) {
            //     console.log('Last inserted id is: ' + row['id']);
            //     res.send(row['id']);
            // });
            res.status(200).json(this.lastID);
            //res.send(this.lastID);
            // res.redirect('/');
        }
    );
});

router.put('/editCorpuses/updateCorpus/:id', function(req, res, next) {
    console.log(req.body)
    db.run(`UPDATE corpus SET name='${req.body.name}', description='${req.body.description}', full_description='${req.body.full_description}' WHERE id=?;`, req.params.id,
        (err) => {
            if (err) { console.error(err.message); }
            // res.redirect('/');
            res.send();
        }
    );
});

router.delete('/editCorpuses/deleteCorpus/:id', function(req, res, next) {
    db.run(`DELETE FROM corpus WHERE id=?;`, req.params.id,
        (err) => {
            if (err) { console.error(err.message); }
            // res.redirect('/');
            res.send();
        }
    );
});

router.get('/search/getStoriesContent', function(req, res, next) {
    db.all(`SELECT story_content.transcription as string, story.id as storyId, story.name as storyName, corpus.name
            FROM story_content 
            INNER JOIN story ON story.id=story_content.story_id  
            INNER JOIN corpus ON corpus.id=story.corpus_id`, (err, rows) => {
        if (err) { console.error(err.message); }
        // var result = [];
        // rows.forEach(row => {
        //     result.push({ id: row.id, name: row.name, description: row.description });
        // });
        res.send(rows)
    });
});

router.post('/addstory', uploadAudio.single('file'), function(req, res, next) {
    db.run(`INSERT INTO story(name, corpus_id, description, link_on_file) VALUES ('${req.body.name}', '${req.body.corpus_id}', '${req.body.description}', '${req.file.path.substring(10, req.file.path.length)}');`,
        function (err) {
            if (err) { console.error(err.message); }
            res.status(200).json(this.lastID);
        }
    );
})

router.put('/updateStory/:id', uploadAudio.single('file'), function(req, res, next) {
    if (req.file){
        // сначала получаем ссылку на старый файл
        db.all('SELECT link_on_file FROM story WHERE id=?', req.params.id, (err, rows) => {
            if (err) { console.error(err.message); }
            var link_on_file = "";
            link_on_file = rows[0].link_on_file;
            fs.unlink(link_on_file, function(err){
                if (err) {
                    console.log(err);
                } else {
                    console.log("Файл удалён");
                }
            });
            db.run(`UPDATE story SET name='${req.body.name}', corpus_id='${req.body.corpus_id}', description='${req.body.description}', link_on_file='${req.file.path.substring(10, req.file.path.length)}' WHERE id=?;`, req.params.id,
                function (err) {
                    if (err) { console.error(err.message); }
                    res.send();
                }
            );

        });
    }
    else{
        // сначала получаем ссылку на старый файл
        db.all('SELECT link_on_file FROM story WHERE id=?', req.params.id, (err, rows) => {
            if (err) { console.error(err.message); }
            var link_on_file = "";
            link_on_file = rows[0].link_on_file;
            fs.unlink(link_on_file, function(err){
                if (err) {
                    console.log(err);
                } else {
                    console.log("Файл удалён");
                }
            });
            db.run(`UPDATE story SET name='${req.body.name}', corpus_id='${req.body.corpus_id}', description='${req.body.description}' WHERE id=?;`, req.params.id,
                function (err) {
                    if (err) { console.error(err.message); }
                    res.send();
                }
            );

        });
    }
})

router.delete('/deleteStory/:id', function(req, res, next) {
    db.all('SELECT link_on_file FROM story WHERE id=?', req.params.id, (err, rows) => {
        if (err) { console.error(err.message); }
        var link_on_file = "";
        link_on_file = rows[0].link_on_file;
        fs.unlink(link_on_file, function(err){
            if (err) {
                console.log(err);
            } else {
                console.log("Файл удалён");
            }
        });
        db.run(`DELETE FROM story WHERE id=?;`, req.params.id,
            function (err) {
                console.log(this)
                if (err) { console.error(err.message); }
                res.send();
            }
        );
    });
});

String.prototype.replaceAll = function(search, replace){
    return this.split(search).join(replace);
}


router.get('/story/:id', function(req, res, next) {
    db.all(`SELECT story_content.*, story.name as storyName, story.corpus_id as corpus_id
            FROM story_content
            INNER JOIN story ON story.id=story_content.story_id
            WHERE story_id=?`, req.params.id, (err, rows) => {
        if (err) { console.error(err.message); }
        var result = [];
        rows.forEach(row => {
            result.push({ id: row.id, time_begin: row.time_begin, time_end: row.time_end,
                comment: row.comment, transcription: row.transcription, storyName: row.storyName,
                corpus_id: row.corpus_id
            });
        });
        // получаем ссылку на файл
        db.all('SELECT link_on_file FROM story WHERE id=?', req.params.id, (err, rows) => {
            if (err) { console.error(err.message); }
            var link_on_file = "";
            link_on_file = rows[0].link_on_file;

            if(link_on_file!=""){
                link_on_file = link_on_file.replace(/\\/g, '/');
            }
            res.render('story', { title: 'Детали рассказа', list: result, link_on_file: link_on_file, story_id: req.params.id });
        });

    });
});

router.get('/story/:id/gettable', function(req, res, next) {
    db.all(`SELECT story_content.*, story.name as storyName, story.corpus_id as corpus_id
            FROM story_content
            INNER JOIN story ON story.id=story_content.story_id
            WHERE story_id=?`, req.params.id, (err, rows) => {
        if (err) { console.error(err.message); }
        var result = [];
        var storyName="";
        storyName = rows[0].storyName;
        rows.forEach(row => {
            result.push({ id: row.id, time_begin: row.time_begin, time_end: row.time_end,
                transcription: row.transcription, comment: row.comment, story_id: row.story_id, storyName: row.storyName,
                corpus_id: row.corpus_id });
        });
        db.all('SELECT link_on_file FROM story WHERE id=?', req.params.id, (err, rows) => {
            if (err) { console.error(err.message); }
            var link_on_file = "";
            link_on_file = rows[0].link_on_file;
            res.send({list: result, id: req.params.id, link_on_file: link_on_file, storyName: storyName});
        });
        
    });
});

router.get('/glossary', function(req, res, next) {
    db.all('SELECT * FROM dictionary ORDER BY name', (err, rows) => {
        if (err) { console.error(err.message); }
        var result = [];
        rows.forEach(row => {
            result.push({ id: row.id, name: row.name, description: row.description});
        });
        res.render('glossary', { title: 'Глоссарий', list:result});
    });

})

router.get('/editGlossary', function(req, res, next) {
    db.all('SELECT * FROM dictionary ORDER BY name', (err, rows) => {
        if (err) { console.error(err.message); }
        var result = [];
        rows.forEach(row => {
            result.push({ id: row.id, name: row.name, description: row.description});
        });
        res.render('editGlossary', { title: 'Редактирование глоссария', list:result});
    });

})

router.post('/addWord', function(req, res, next) {
    console.log(req.body);
    db.run(`INSERT INTO dictionary(name, description) VALUES ('${req.body.name}', '${req.body.description}');`,
        (err) => {
            if (err) { console.error(err.message); }
            res.redirect('/editGlossary');
        }
    );
})

router.get('/editWord/:id', function(req, res, next) {
    db.all('SELECT * FROM dictionary WHERE id=?', req.params.id, (err, rows) => {
        if (err) { console.error(err.message); }
        res.render('editWord', { title: 'Редактирование слова', word: rows[0]});
    });
});

router.post('/deleteWord/:id', function(req, res, next) {
    db.run(`DELETE FROM dictionary WHERE id=?;`, req.params.id,
        (err) => {
            if (err) { console.error(err.message); }
            res.redirect('/editGlossary');
        }
    );
});
router.post('/updateWord/:id', function(req, res, next) {
    console.log(req.body)
    db.run(`UPDATE dictionary SET name='${req.body.name}', description='${req.body.description}' WHERE id=?;`, req.params.id,
        (err) => {
            if (err) { console.error(err.message); }
            res.redirect('/editGlossary');
        }
    );
});



// router.get('/stories', function(req, res, next) {
//     db.all('SELECT * FROM story', (err, rows) => {
//         if (err) { console.error(err.message); }
//         res.send(rows)
//     });
// })

// router.get('/story/:id', function(req, res, next) {
//     db.all('SELECT * FROM story WHERE id=?', req.params.id, (err, rows) => {
//         if (err) { console.error(err.message); }
//         res.send(rows)
//     });
// });


// router.post('/addstory', function(req, res, next) {
//     db.run(`INSERT INTO story(name, corpus_id) VALUES ('${req.body.name}', '${req.body.corpus_id}');`,
//         (err) => {
//             if (err) { console.error(err.message); }
//             // res.redirect('/');
//         }
//     );
// })

// router.put('/upadteStory/:id', function(req, res, next) {
//     db.run(`UPDATE story SET name='${req.body.name}', corpus_id='${req.body.corpus_id}' WHERE id=?;`, req.params.id,
//         (err) => {
//             if (err) { console.error(err.message); }
//             res.redirect('/');
//         }
//     );
// })

// router.delete('/deleteStory/:id', function(req, res, next) {
//     db.run(`DELETE FROM story WHERE id=?;`, req.params.id,
//         (err) => {
//             if (err) { console.error(err.message); }
//             res.redirect('/');
//         }
//     );
// });


//здесь происходит сама загрузка
router.post('/downloadFile', function(req, res, next) {
    // создаем форму
    var form = new multiparty.Form();
    //здесь будет храниться путь с загружаемому файлу, его тип и размер
    var uploadFile = {uploadPath: '', type: '', size: 0};

    //массив с ошибками произошедшими в ходе загрузки файла
    var errors = [];

    //если произошла ошибка
    form.on('error', function(err){
        if(fs.existsSync(uploadFile.path)) {
            //если загружаемый файл существует удаляем его
            fs.unlinkSync(uploadFile.path);
            console.log('error');
        }
    });

    form.on('close', function() {
        //если нет ошибок
        if(errors.length == 0) {

            res.render('index', { title: 'Список команд'});
        }
        else {
            if(fs.existsSync(uploadFile.path)) {
                //если загружаемый файл существует удаляем его
                fs.unlinkSync(uploadFile.path);
            }
            //сообщаем что все плохо и какие произошли ошибки
            res.send({status: 'bad', errors: errors});
        }
    });

    // при поступление файла
    form.on('part', function(part) {
        //читаем его размер в байтах
        uploadFile.size = part.byteCount;
        //читаем его тип
        uploadFile.type = part.headers['content-type'];
        //путь для сохранения файла

        //для того чтобы не было одинаковых имён у изображений, в название будем вставлять случайную последовательность из 6 символов
        var filename_ = randomString(6) + part.filename;

        uploadFile.path = './files/' + filename_;

        //если нет ошибок то создаем поток для записи файла
        if(errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            part.pipe(out);
        }
        else {
            //пропускаем
            //вообще здесь нужно как-то остановить загрузку и перейти к onclose
            part.resume();
        }
    });

    // парсим форму
    form.parse(req);
});

//используется для генерации случайной строки
function randomString(count) {
    var rnd = '';
    while (rnd.length < count)
        rnd += Math.random().toString(36).substring(2);
    return rnd.substring(0, count);
};

module.exports = router;
