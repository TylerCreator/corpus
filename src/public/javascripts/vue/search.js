var search = new Vue({
    el: '#search',
    data: {
        adress: window.location.protocol +'//'+ window.location.host,
        rows: [],
        total: 0,
        search: ''
    },
    created(){
        axios.get(window.location.toString()+'/getStoriesContent').then(
            response =>{
                this.rows = response.data
            }
        )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
    },
    computed: {
        filteredRows() {
            let map = new Map();
            var self = this
            var filtered = []
            
            if (self.search != ''){
                var re = new RegExp("(^|[^_0-9a-zA-Zа-яёА-ЯЁ])"+self.search+"([^_0-9a-zA-Zа-яёА-ЯЁ]|$)",'gi');
                filtered = self.rows;
                filtered = filtered.filter(function (row) {

                    if (row.string.search(re) > -1) {
                        let id = row.storyId;
                        if (map.has(id)){
                            count = map.get(id)
                            map.set(id,[[row.name, row.storyName, id], count[1]+1])
                        }
                        else{
                            map.set(id,[[row.name, row.storyName, id], 1])
                        }
                        return true
                    }
                    return false
                })
            }
            this.total = filtered.length
            return map;
        }
    }
})