var table = new Vue({
    el: '#table',
    data: {
        adress: window.location.protocol +'//'+ window.location.host,
        rows: [],
        indexRow: 0,
        t_begin: '',
        t_end: '',
        ede: '',
        comment: '',
        newT_begin: '',
        newT_end: '',
        newEde: '',
        newComment: '',
        link_on_file: null,
        story_id: null,
        storyName: null,
        label_row_left: null,
        label_row_right: null,
    },
    created(){
        axios.get(window.location.toString()+'/gettable').then(
            response =>{
                this.rows = response.data.list
                this.story_id = response.data.id
                this.link_on_file = response.data.link_on_file
                this.storyName = response.data.storyName
                // String.prototype.replaceAll = function(search, replace){
                //     return this.split(search).join(replace);
                // }
                // if (response.data.link_on_file!==undefined) {
                //     var str = "src\\public";
                //     var link_on_file_ = response.data.link_on_file;
                //     link_on_file_ = link_on_file_.replace(str,"");
                //     this.link_on_file = link_on_file.replaceAll("\\","/");
                // }
                // else{
                //     this.link_on_file = ""
                // }

            }
        )
        .catch(
            e =>{
                console.log(e)
                document.write(e.response.data)
            }
        )
    },
    methods: {
        labelRow(index){
            if(this.label_row_left !== null){
                if(this.label_row_right  !== null){
                    this.label_row_left = null
                    this.label_row_right = null
                }
                else{
                    this.label_row_right = Math.max(index,this.label_row_left)
                    this.label_row_left = Math.min(index,this.label_row_left)
                }
            }
            else{
                this.label_row_left = index
            }
        },
        addRow(){
            //тут отправка на сервер
            axios.post(this.adress+'/addTable', {
                time_begin: this.t_begin,
                time_end: this.t_end,
                transcription: this.ede,
                comment: this.comment,
                story_id: this.story_id
            })
            .then(
                response =>{
                    id = response.data
                    this.rows.push({
                        id: id,
                        time_begin: this.t_begin,
                        time_end: this.t_end,
                        transcription: this.ede,
                        comment: this.comment,
                    })
                    this.t_begin = '',
                    this.t_end = '',
                    this.ede = '',
                    this.comment = '',
                    console.log(response.data)
                    //window.location=this.adress
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
        },
        selectRow(index){
            this.indexRow = index
            this.newT_begin = this.rows[index].time_begin,
            this.newT_end = this.rows[index].time_end,
            this.newEde = this.rows[index].transcription,
            this.newComment = this.rows[index].comment
        },
        deleteRow(){
            //тут отправка на сервер
            axios.delete(this.adress+'/deleteTable/'+this.rows[this.indexRow].id)
            .then(
                //sessionStorage.reloadAfterPageLoad = true,
                response =>{
                    this.rows.splice(this.indexRow, 1)
                    console.log(response.data)
                    //window.location=this.adress
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
        },
        saveRow(){
            // тут отправка на сервер
            axios.put(this.adress+'/updateTable/'+this.rows[this.indexRow].id, {
                time_begin: this.newT_begin,
                time_end: this.newT_end,
                transcription: this.newEde,
                comment: this.newComment,
                story_id: this.story_id
            })
            .then(
                response =>{
                    this.rows[this.indexRow].time_begin = this.newT_begin
                    this.rows[this.indexRow].time_end = this.newT_end
                    this.rows[this.indexRow].transcription = this.newEde
                    this.rows[this.indexRow].comment = this.newComment
                    console.log(response.data)
                    //window.location=this.adress
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
        },
    }
})