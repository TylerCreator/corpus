var story = new Vue({
    el: '#story',
    data: {
        adress: window.location.protocol +'//'+ window.location.host,
        stories: [],
        indexStory: 0,
        name: '',
        description: '',
        newName: '',
        newDescription: '',
        corpus_id: null,
        selectedFile: null,
        newSelectedFile: null,
    },
    created(){
        axios.get(window.location.toString()+'/getcorpus').then(
            response =>{
                this.stories = response.data.list
                this.corpus_id = response.data.id
            }
        )
        .catch(
            e =>{
                console.log(e)
                document.write(e.response.data)
            }
        )
    },
    methods: {
        onFileChanged (event) {
            this.selectedFile = event.target.files[0]
        },
        onNewFileChanged (event) {
            this.newSelectedFile = event.target.files[0]
        },
        addStory(){
            //тут отправка на сервер
            const formData = new FormData()
            formData.append('file', this.selectedFile, this.selectedFile.name)
            formData.append('name', this.name)
            formData.append('corpus_id', this.corpus_id)
            formData.append('description', this.description)
            // axios.post(this.adress+'/addStory', {
            //     file: formData,
            //     name: this.name,
            //     corpus_id: this.corpus_id,
            //     description: this.description
            // })
            axios.post(this.adress+'/addStory', formData)
            .then(
                response =>{
                    id = response.data
                    this.stories.push({
                        id: id,
                        name: this.name,
                        description: this.description
                    })
                    this.name = ''
                    this.description = ''
                    this.selectedFile = null
                    this.$refs.myFileInput1.value = ''
                    console.log(response.data)
                    //window.location=this.adress
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
        },
        selectStory(index){
            this.indexStory = index
            this.newName = this.stories[index].name
            this.newDescription = this.stories[index].description
            this.newSelectedFile = null
            this.$refs.myFileInput2.value = ''
        },
        deleteStory(){
            //тут отправка на сервер
            axios.delete(this.adress+'/deleteStory/'+this.stories[this.indexStory].id)
            .then(
                //sessionStorage.reloadAfterPageLoad = true,
                response =>{
                    this.stories.splice(this.indexStory, 1)
                    console.log(response.data)
                    //window.location=this.adress
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
        },
        saveStory(){
            // тут отправка на сервер
            const formData = new FormData()
            if (this.newSelectedFile){
                formData.append('file', this.newSelectedFile, this.newSelectedFile.name)
            }
            else{
                formData.append('file', null)
            }
            formData.append('name', this.newName)
            formData.append('corpus_id', this.corpus_id)
            formData.append('description', this.newDescription)
            axios.put(this.adress+'/updateStory/'+this.stories[this.indexStory].id, formData)
            .then(
                response =>{
                    this.stories[this.indexStory].description = this.newDescription
                    this.stories[this.indexStory].name = this.newName
                    console.log(response.data)
                    //window.location=this.adress
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
        },
    }
})