var corpus = new Vue({
    el: '#corpus',
    data: {
        adress: window.location.toString(),
        corpuses: [],
        indexCorpus: 0,
        name: '',
        description: '',
        newName: '',
        newDescription: '',
    },
    created(){
        axios.get(this.adress+'/getcorpuses').then(
            response =>{
                this.corpuses = response.data
            }
        )
        .catch(
            e =>{
                console.log(e)
                document.write(e.response.data)
            }
        )
    },
    methods: {
        addCorpus(){
            //тут отправка на сервер
            axios.post(this.adress+'/addCorpus', {
                name: this.name,
                description: this.description
            })
            .then(
                response =>{
                    id = response.data
                    this.corpuses.push({
                        id: id,
                        name: this.name,
                        description: this.description
                    })
                    this.name = ''
                    this.description = ''
                    console.log(response.data)
                    //window.location=this.adress 
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )  
        },
        selectCorpus(index){
            this.indexCorpus = index
            this.newName = this.corpuses[index].name
            this.newDescription = this.corpuses[index].description
        },
        deleteCorpus(){
            //тут отправка на сервер
            axios.delete(this.adress+'/deleteCorpus/'+this.corpuses[this.indexCorpus].id)
            .then(
                //sessionStorage.reloadAfterPageLoad = true,
                response =>{
                    this.corpuses.splice(this.indexCorpus, 1)
                    console.log(response.data)
                    //window.location=this.adress 
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )  
        },
        saveCorpus(){
            // тут отправка на сервер
            axios.put(this.adress+'/updateCorpus/'+this.corpuses[this.indexCorpus].id, {
                name: this.newName,
                description: this.newDescription
            })
            .then(
                response =>{
                    this.corpuses[this.indexCorpus].description = this.newDescription
                    this.corpuses[this.indexCorpus].name = this.newName
                    console.log(response.data)
                    //window.location=this.adress 
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )  
        },
    }
})