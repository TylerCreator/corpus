var page = new Vue({
    el: '#page',
    data: {
        /* Your data and models here */
        adress: window.location.toString(),
        myModel: '<p>Привет, мир!</p>',

        /* Config can be declare here */
        myPlugins : [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker advlist anchor  autolink charmap codesample colorpicker contextmenu directionality',
                     'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking fullscreen help hr image imagetools insertdatetime link lists media noneditable',
                     'save table contextmenu directionality emoticons template paste textcolor image imagetools  pagebreak preview print searchreplace table template textcolor textpattern toc visualblocks visualchars wordcount image media'
        ],
        myToolbar1: 'insertfile undo redo | styleselect fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons', 
        myToolbar2 : '',
        myOtherOptions : {
            height: 200,
            theme: 'modern',
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            //automatic_uploads: false,
            language: 'ru',
            language_url : '../tinymce/langs/ru.js',
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
            images_upload_url: window.location.toString()+'/sendimage',
            //,width:600,
            //directionality: 'rtl',
            //theme: 'modern',
            //menubar: false
            //, etc...
        }
    },
    components: {
        'tinymce': VueEasyTinyMCE
    },
    created(){
        axios.get(this.adress+'/getmainpage').then(
            response =>{
                this.myModel = response.data
            }
        )
        .catch(
            e =>{
                console.log(e)
                document.write(e.response.data)
            }
        )
    },
    methods: {
        getModelValue: function () {
            alert(this.myModel);
        },
        postPage(){
            if(this.myModel==''){
                alert("Введите данные!")
                return
            }
            axios.post(this.adress+'/setmainpage', {
                html: this.myModel
            })
            .then(
                response =>{
                    window.location= '/'
                }
            )
            .catch(
                e =>{
                    console.log(e)
                    document.write(e.response.data)
                }
            )
        }
    }
});
