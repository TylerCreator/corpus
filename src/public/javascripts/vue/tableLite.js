var tableLite = new Vue({
    el: '#tableLite',
    data: {
        adress: window.location.protocol +'//'+ window.location.host,
        rows: [],
        link_on_file: null,
        story_id: null,
        label_row_left: null,
        label_row_right: null,
    },
    created(){
        axios.get(window.location.toString()+'/gettable').then(
            response =>{
                this.rows = response.data.list
                this.story_id = response.data.id
                this.link_on_file = response.data.link_on_file
            }
        )
        .catch(
            e =>{
                console.log(e)
                document.write(e.response.data)
            }
        )
    },
    methods: {
        labelRow(index){
            if(this.label_row_left !== null){
                if(this.label_row_right  !== null){
                    this.label_row_left = null
                    this.label_row_right = null
                }
                else{
                    this.label_row_right = Math.max(index,this.label_row_left)
                    this.label_row_left = Math.min(index,this.label_row_left)
                }
            }
            else{
                this.label_row_left = index
            }
        }
    }
})