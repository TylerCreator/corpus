$(document).ready(function(){
    var select_start_index = -1;
    $("#samples_data_div").on('click', '#table tbody tr', function(e) {
        var t_rows = $(this).parent().find('tr');
        if (!e.shiftKey && !e.ctrlKey) {
            t_rows.removeClass('activeTr');
            $(this).addClass('activeTr');
            select_start_index = t_rows.index(this);
        }

        if (e.ctrlKey) {
            if ($(this).hasClass('activeTr')) {
                $(this).removeClass('activeTr');
            } else {
                $(this).addClass('activeTr');
            }
        }

        if (e.shiftKey) {
            var select_end_index = t_rows.index(this);
            var each_start, each_end;
            if(select_start_index != -1){
                if(select_start_index < select_end_index){
                    each_start = select_start_index;
                    each_end = select_end_index;
                }
                else{
                    each_start = select_end_index;
                    each_end = select_start_index;
                }
                t_rows.each(function(index){
                    if(index >= each_start && index <= each_end){
                        $(this).addClass('activeTr');
                    }
                });
            }
        }

        if ($(".activeTr").length > 1){
            var length = $(".activeTr").length;
            var time_begin = $(".activeTr .time_begin")[0].innerHTML;
            var time_end = $(".activeTr .time_end")[length-1].innerHTML;
            var link_on_file = $("#link_on_file")[0].innerHTML;
            playAudioFragment(link_on_file, time_begin, time_end)
        }
    });

    var audio = document.getElementById("player");

    audio.onpause = function() {
        var playButtons = document.getElementsByTagName("button");
        for (var i=0; i < playButtons.length; i++) {
            playButton = playButtons[i];
            playButton.innerHTML = "<i class=\"fa fa-play\"></i>";
        }
    }

    audio.onplay = function() {
        var playButtons = document.getElementsByTagName("button");
        for (var i=0; i < playButtons.length; i++) {
            playButton = playButtons[i];
            playButton.innerHTML = "<i class=\"fa fa-pause\"></i>";
        }
    }

    function playAudioFragment(pathToFile, begin, end) {
        if (audio.paused) {
            audio.src = pathToFile + "#t=" + begin + "," + end;
            audio.play();
        } else {
            audio.pause();
        }
    }

    $(document).mouseup(function (e){
        var div = $("#table tbody tr");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.removeClass('activeTr');
            audio.pause();
        }
    });
});






