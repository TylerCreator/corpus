BEGIN TRANSACTION;
CREATE TABLE "story_content" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`time_begin`	TEXT,
	`time_end`	TEXT,
	`ni`	REAL,
	`eb`	TEXT,
	`ol`	TEXT,
	`comment`	TEXT,
	`story_id`	INTEGER,
	`link_on_file`	TEXT,
	FOREIGN KEY(`story_id`) REFERENCES `story`(`id`)
);
CREATE TABLE "story" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT,
	`corpus_id`	INTEGER,
	FOREIGN KEY(`corpus_id`) REFERENCES `corpus`(`id`)
);
CREATE TABLE `dictionary` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT
);
CREATE TABLE `corpus` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT,
	`description`	TEXT,
	`full_desciption`	TEXT
);
COMMIT;
